<?php

namespace Norkaan\NotificationSetting\Providers;

use Illuminate\Console\Events\CommandFinished;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Console\Output\StreamOutput;

class NotificationSettingEventProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    	// and event to launch command for notification type creation in database after a make:notification
	    Event::listen('Illuminate\Console\Events\CommandFinished', function (CommandFinished $event) {

		    if($event->command == 'make:notification'){
			    $stream = fopen("php://output", "w");
			    Artisan::call("marqueblanche:insertNotificationType " . $event->input->getArguments()['name'], array(), new StreamOutput($stream));
		    }
	    });
    }
}
