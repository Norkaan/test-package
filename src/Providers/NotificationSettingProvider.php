<?php

namespace Norkaan\NotificationSetting\Providers;

use Norkaan\NotificationSetting\Console\Commands\InsertNotificationTypeCommand;
use Illuminate\Support\ServiceProvider;

class NotificationSettingProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->register(NotificationSettingEventProvider::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    	// load migration file to create setting and type notifications table
	    $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
	    $this->commands([
		                    InsertNotificationTypeCommand::class,
	                    ]);
    }
}
