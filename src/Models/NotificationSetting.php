<?php

namespace Norkaan\NotificationSetting\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationSetting extends Model
{
    use HasFactory;
    protected $fillable = ['type', 'via'];

    protected $casts = ['via' => 'array'];

    public function notifiable()
    {
        return $this->morphTo();
    }


    public function type(){
        return $this->belongsTo('Norkaan\NotificationSetting\Models\NotificationType');
    }

}
