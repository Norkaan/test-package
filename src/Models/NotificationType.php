<?php

namespace Norkaan\NotificationSetting\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationType extends Model
{
    use HasFactory;
    protected $fillable = ['type', 'via'];

    protected $casts = ['via' => 'array'];

	public function notificationSettings(){
		return $this->hasMany('Norkaan\NotificationSetting\Models\NotificationSetting');
	}

    public $timestamps = false;
}
