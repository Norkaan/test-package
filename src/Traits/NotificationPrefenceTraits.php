<?php

namespace Norkaan\NotificationSetting\Traits;


use Norkaan\NotificationSetting\Exceptions\NotificationTypeException;
use Norkaan\NotificationSetting\Models\NotificationSetting;
use Norkaan\NotificationSetting\Models\NotificationType;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
//use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Arr;

trait NotificationPrefenceTraits
{
	/**
	 * @param $notificationClass
	 *
	 * @return string[] Return list of channel default is empty
	 */
    public function getViaForNotification($notificationClass) {
        $settingForNotification = $this->notificationSettings()->where('type', $notificationClass)->first();
        if($settingForNotification){
            return $settingForNotification->via;
        } else {
            return [];
        }
    }

    public function notificationSettings()
    {
        return $this->morphMany(NotificationSetting::class, 'notifiable')->orderBy('created_at', 'desc');
    }

    /**
     * @param $notificationClass
     * @param string[] channels|true|false $via
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function updateNotificationSetting($notificationClass, $via){

        $notificationType = NotificationType::where( 'type', $notificationClass )->first();
        if ( $notificationType ) {
        	// $notificationType->transformVia($via);?
	        if($via === false){
	            $via = [];
	        } elseif ( $via === true ) {
                $via = $notificationType->via;
            } else {
                $via = Arr::flatten(array_intersect($via, $notificationType->via));
            }
        } else {
            throw new NotificationTypeException( 'Notification type ' . $notificationClass . ' not exist' );
        }

        return $this->notificationSettings()->where('type', $notificationClass)->updateOrCreate(['type' => $notificationClass], ['via'=>$via]);
    }

	/**
	 * @param String[] $via List of channel
	 *
	 * @throws NotificationTypeException
	 */
    public function updateAllNotificationSetting($via){
    	$notificationsTypes = NotificationType::all();
    	foreach ($notificationsTypes as $notificationType){
    		$this->updateNotificationSetting($notificationType->type, $via);
	    }
    }

    public function notificationType(){
        return $this->belongsTo('App\Models\NotificationType', 'type', 'type');

    }
}
