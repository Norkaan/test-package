<?php

namespace Norkaan\NotificationSetting\Console\Commands;

use Illuminate\Console\Command;
use Norkaan\NotificationSetting\Models\NotificationType;

class InsertNotificationTypeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marqueblanche:insertNotificationType {notificationType}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insère un type de notification dans la table des types de l\'application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $notificationType = $this->argument('notificationType');
        $defaultIndex = 0;
        $availableChannels = [
        	'database',
	        'mail',
	        'mail,database',
	        'database,mail',
        ];
        $via = $this->anticipate(
            'Voulez vous créer la notification '.$notificationType.' dans les types disponible de l\'application ? Si oui, indiquez sur quels canaux, séparés par des virgule [défaut : no]',
            array_merge($availableChannels)
        );

        if(!empty($via) && $via != 'no' ){

            NotificationType::updateOrCreate(
                ['type' => $notificationType],
                ['via' => explode(',',$via)]
            );
            return true;
        } else {

            return false;
        }
    }
}
