<?php

namespace Norkaan\NotificationSetting\Exceptions;

use Exception;

class NotificationTypeException extends Exception {}
