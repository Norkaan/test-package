Utilisation

Ajouter le type de notification dans l'application (la commande se lance automatiquement à la creation d'une notification)

    php artisan marqueblanche:insertNotificationType {notificationType}

Pour savoir si l'utilisateur accepte de recevoir la notification en cours utiliser la fonction `getViaForNotification` en passant la classe elle même en paramètre dans la fonction `via` de la notification


    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array

    */
    public function via($notifiable)
    {
        return $notifiable->getViaForNotification(self::class);
    }

## Paramétrer toute les notifications

    $user = User::find(1);
    $user->updateAllNotificationSetting(false); // refuse toute les notifications
    $user->updateAllNotificationSetting(true); // accepte toute les notifications avec les paramètres par défaut
    $user->updateAllNotificationSetting(['mail', 'database']); // Modifie le "via" des notification pour l'utilisateur les paramètre non autorisé pour le type de notification sont supprimés

## Paramétrer une notification
    $user = User::find(1);
    // Possibilité d'utiliser 
    // soit la chaine : 'App\Notification\TestNotification'
    // soit la classe directement : TestNotification::class 
    $user->updateNotificationSetting('App\Notification\TestNotification',false); // refuse les notifications de ce type
    $user->updateNotificationSetting('App\Notification\TestNotification',true); // accepte les notifications de ce type
    $user->updateNotificationSetting('App\Notification\TestNotification',['email', 'database']); // Modifie le "via" de ce type de notification pour l'utilisateur les paramètre non autorisé pour le type de notification sont supprimés
